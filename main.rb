require 'ffi'
require 'objspace'

module MyLib
  extend FFI::Library

  ffi_lib 'c'
  ffi_lib 'target/release/libpoint.so'

  attach_function :point_new, [:double, :double], :pointer
  attach_function :point_x, [:pointer], :double
  attach_function :point_y, [:pointer], :double
  attach_function :point_delete, [:pointer], :void
end

class Point
  def initialize(x, y)
    @p = MyLib::point_new x, y
    ObjectSpace.define_finalizer(self, method(:finalize))
  end

  def finalize
    proc { MyLib::point_delete @p }
  end

  def to_s
    "(#{x}, #{y})"
  end

  def x
    MyLib::point_x @p
  end

  def y
    MyLib::point_y @p
  end
end

if __FILE__ == $0 then
  p = Point.new(6.0, 4.0)
  puts p
end
