#pragma once

#ifdef __cplusplus
extern "C" {
#endif

    void * point_new(double, double);
    double point_x(void *);
    double point_y(void *);
    void point_delete(void *);

#ifdef __cplusplus
}
#endif
