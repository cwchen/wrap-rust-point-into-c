#include <stdio.h>
#include "point.h"

int main(void)
{
    void *p = point_new(6, 4);

    printf("(%.2f, %.2f)\n", point_x(p), point_y(p));

    /* The C program is responsible to
        release system resource.       */
    point_delete(p);

    return 0;
}
