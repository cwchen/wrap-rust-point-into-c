from cffi import FFI

ffi = FFI()

ffi.cdef("""
void * point_new(double, double);
double point_x(void *);
double point_y(void *);
void point_delete(void*);
""")

libpoint = ffi.dlopen('target/release/libpoint.so')

class Point:
    def __init__(self, x, y):
        self.p = libpoint.point_new(x, y)

    def __del__(self):
        libpoint.point_delete(self.p)

    def __str__(self):
        return "({}, {})".format(self.x(), self.y())

    def x(self):
        return libpoint.point_x(self.p)

    def y(self):
        return libpoint.point_y(self.p)

if __name__ == '__main__':
    p = Point(6.0, 4.0)
    print(p)
